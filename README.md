## OSXUtils
#### 一些随手写的终端工具..........

**NO.1 osxCMD**

##### description: 一个随手代码堆叠而成的小工具, 解决一些偶尔用到的小功能.
##### 备注: `libOSXDylib.dylib`,`osxCMD`需要在同目录

##### usage: 
> 基本用法: 

>`osxCMD`查看当前版本支持的功能模块

> `osxCMD <模块名>`查看对应模块支持的功能, `<xxxx>`为必需参数`[xxxxxx]`为可选参数

##### 模块说明:

**`osxCMD SCFormatter` 文本格式化模块**

> `osxCMD SCFormatter plist2json <file> [json_save_path]` 转换PLIST为JSON, `json_save_path`为转换后文件保存路径

> `osxCMD SCFormatter json2plist <file> [plist_save_path]` 转换JSON为PLIST, `json_save_path`为转换后文件保存路径

> `osxCMD SCFormatter jsonformatter [--file=<plist/json_file>] [--string=<plist/json_string>] --mode=<expand/compress> [--xpath-query=<xpath>] [--save-to=<result_save_path>]` 按JSON格式打印, `--file`表示从文件读入, `--string`表示从参数读入, `--mode`输出样式,expand为展开样式,compress为压缩样式(去除空格和换行),`--xpath-query`表示XPATH路径,`--save-to`表示把结果保存到文件

**`osxCMD SCCodeHelper` 代码格式化模块**
> `osxCMD SCCodeHelper fixHeaders <folder_path> [--check-only] [--file-types=<file_type_json_array>] [--ignore-prefix=<ignore_dirs_json_array>] [--nofix-prefix=<nofix_dirs_json_array>] [--nochange-regex=<nochange_regex_json_array>] [--export-list=<file_path>] [--import-list=<file_path>] [--log-file=<file_path>]` 修复头文件引用为相对路径引用, 解决编译时提示XXXX.h找不到问题, 可设置忽略目录/文件
>> * 示例: `osxCMD SCCodeHelper fixHeaders /tmp/myproject_root` 将尝试把`/tmp/myproject_root`目录下所有代码文件的`include/import`头文件引用修复为相对路径引用
>> * 例如 `#include <openssl/openssl.h>` 可能修改为 `#include "../../../include/openssl/openssl.h"`(若相对路径的文件存在则更改, 若相对路径文件不存在则不做任何更改)

**`osxCMD PPHelper` PP助手模块(下载PP助手越狱商店中的指定版本IPA文件, 无需IOS)**

> `osxCMD PPHelper <jbsearch|storesearch> <keyword>` 搜索PP助手商店, `jbsearch`表示搜索越狱商店, `storesearch`表示搜索App商店, `keyword`为搜索关键字

>> * 示例 `osxCMD PPHelper jbsearch 微信` 搜索越狱商店中的微信, 可以看到最新版本的IPA下载链接和`id`

> `osxCMD PPHelper query <pkgId>` 查询某个APP是否有历史版本可用(有时需要下载旧版本的APP), `pkgId`为APP的ID(在上面的命令结果中找到)
>> * 示例: 通过`osxCMD PPHelper jbsearch 微信`搜索微信得到微信的`id`为`620065`, 再通过`osxCMD PPHelper query 620065`即可得到所有可用的历史版本IPA下载链接

**`osxCMD SSLinker` 对某网站抓取免费的SS服务([注册地址](https://ss109.tk/))**
> `osxCMD SSLinker autobuy <username> <password> [path_to_save] [--ss-json=<listen_ip>:<listen_port>]` 自动购买一个免费服务并返回服务器信息, `username`为用户名, `password`为登录密码, `path_to_save`为保存的文件路径, `--ss-json`生成为通用SS客户端JSON格式并指定监听的IP和端口
>> * 示例: `osxCMD SSLinker autobuy abc@qq.com 123456 /tmp/ss.json --ss-json=127.0.0.1:1080`

> `osxCMD SSLinker listMyHosts <username> <password> [path_to_save] [--ss-json=<listen_ip>:<listen_port>]` 查询已购买的服务器信息, 参数说明与购买一致.

**`osxCMD NetUtils` 网络工具模块**
> `osxCMD NetUtils dnsquery <domain> [@<dns_server>] [--ip-only] [--cname-only]` 查询域名对应的DNS记录, `domain`为要查询的域名, `dns_server`为DNS服务器IP, `--ip-only`表示只显示A记录, `--cname-only`表示只显示CNAME记录
>> * 示例: `osxCMD NetUtils dnsquery www.baidu.com @114.114.114.114`

> `osxCMD NetUtils ping <domain/ip> [--count=<num>] [--timeout=<num>] [--interval=<num>] [--format=<format>]` ping指定域名/IP, `--count`指定PING的次数, `--timeout`指定超时时长, `--interval`指定每次PING的间隔, `--format`指定要显示的数据, 参考对应占位符
>> * 示例: `osxCMD NetUtils ping www.baidu.com --count=5 '--format=result: $PINGF(ms)'`注意$是SHELL的特殊字符, 需要用特别处理

**`osxCMD TextUtils` 文本工具模块**
> `osxCMD TextUtils removeDupline <file> <dest> [only_check_keyword_line]` 删除文件中重复的行并保存为新文件, `file`为源文件, `dest`为保存的新文件, `only_check_keyword_line`表示只搜索包含对应关键字的行.
>> * 示例: `osxCMD TextUtils removeDupline /tmp/1.txt /tmp/2.txt UserName` 删除包含UserName关键字的重复行

**`osxCMD LXEHelper` 楼小二免APP开闸模块**
> `osxCMD LXEHelper login <user_name> <password>` 用户登录接口, 主要是为了拿到UserId<br>
> `osxCMD LXEHelper citylist` 获取可用的城市列表<br>
> `osxCMD LXEHelper buildinglist <city_code>` 获取指定城市的可用闸门列表, `city_code`为城市代码, 在城市列表中找到<br>
> `osxCMD LXEHelper qrstring <user_id> <building_code> [phone_number] [image_save_path]` 生成二维码对应的字符串, `user_id`为用户ID, `building_code`为楼宇代码, `phone_number`为手机号, `image_save_path`为二维码图片保存路径

**`osxCMD ArchiveTool` 归档文件工具模块**
> `osxCMD ArchiveTool ipainfo <ipa_path> [--xpath-query=<xpath_for_query>] [--save-info=<plist_save_path>]` 查看IPA中Info.plist的文件信息(MEM解压, 不生成临时文件), `ipa_path`为IPA路径, `--xpath-query`以XPATH方式查询某节点(XPATH语法参考SCFormatter), `--save-info`保存信息到新文件


## About
##### 生活如代码, 路只是个if而已.

* Contact Me: qokelate$gmail.com
* Donate Me: (Alipay/WeChat)
* ![Alipay](Alipay.png) ![WeChat](WeChat.png)
